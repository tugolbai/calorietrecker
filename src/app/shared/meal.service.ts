import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs'
import { Meal } from './meal.model';

@Injectable()
export class MealService {
  mealsChange = new Subject<Meal[]>();
  mealsFetching = new Subject<boolean>();
  mealUploading = new Subject<boolean>();
  mealRemoving = new Subject<boolean>();

  private meals: Meal[] = [];

  constructor(private http: HttpClient) {}

  getMeals() {
    return this.meals.slice();
  }

  fetchMeals() {
    this.mealsFetching.next(true);
    this.http.get<{ [id: string]: Meal }>('https://plovo-57f89-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }

        return Object.keys(result).map(id => {
          const mealData = result[id];
          return new Meal(id, mealData.mealTime, mealData.description, mealData.calorie);
        });
      }))
      .subscribe(meals => {
        this.meals = meals;
        this.mealsChange.next(this.meals.slice());
        this.mealsFetching.next(false);
      }, () => {
        this.mealsFetching.next(false);
      });
  }

  fetchMeal(id: string) {
    return this.http.get<Meal | null>(`https://plovo-57f89-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      map(result => {
        if (!result) {
          return null;
        }
        return new Meal(id, result.mealTime, result.description, result.calorie);
      }),
    );
  }

  addMeal(meal: Meal) {
    const body = {
      mealTime: meal.mealTime,
      description: meal.description,
      calorie: meal.calorie,
    };

    this.mealUploading.next(true);

    return this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/meals.json', body).pipe(
      tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      })
    );
  }

  editMeal(meal: Meal) {
    this.mealUploading.next(true);

    const body = {
      mealTime: meal.mealTime,
      description: meal.description,
      calorie: meal.calorie,
    };

    return this.http.put(`https://plovo-57f89-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body).pipe(
      tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      })
    );
  }

  removeMeal(id: string) {
    this.mealRemoving.next(true);

    return this.http.delete(`https://plovo-57f89-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      tap(() => {
        this.mealRemoving.next(false);
      }, () => {
        this.mealRemoving.next(false);
      })
    );
  }
}
