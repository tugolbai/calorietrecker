import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MealsComponent } from './meals/meals.component';
import { MealDetailsComponent } from './meals/meal-details/meal-details.component';
import { EditMealComponent } from './meals/edit-meal/edit-meal.component';
import { MealService } from './shared/meal.service';

@NgModule({
  declarations: [
    AppComponent,
    MealsComponent,
    MealDetailsComponent,
    EditMealComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }
