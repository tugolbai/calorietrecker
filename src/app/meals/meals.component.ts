import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { Subscription } from 'rxjs';
import { MealService } from '../shared/meal.service';

@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit, OnDestroy {
  meals: Meal[] = [];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  isFetching = false;
  calorie = 0;

  constructor(private mealService: MealService) { }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealService.mealsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.mealService.fetchMeals();
  }

  getCalories() {
    this.calorie = 0;
    this.meals.forEach(meal => {
      this.calorie += meal.calorie;
    })
    return this.calorie;
  }

  ngOnDestroy(): void {
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
  }

}
