import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MealService } from '../../shared/meal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Meal } from '../../shared/meal.model';

@Component({
  selector: 'app-edit-meal',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.css']
})
export class EditMealComponent implements OnInit, OnDestroy {
  @ViewChild('f') mealForm!: NgForm;

  isEdit = false;
  editedId = '';

  isUploading = false;
  mealUploadingSubscription!: Subscription;


  constructor(
    private mealService: MealService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.mealUploadingSubscription = this.mealService.mealUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });

    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.dish;

      if (meal) {
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          mealTime: meal.mealTime,
          description: meal.description,
          mealCalorie: meal.calorie
        });

      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          mealTime: '',
          description: '',
          mealCalorie: 0,
        });
      }
    });
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    });
  }

  saveMeal() {
    const id = this.editedId || Math.random().toString();

    const meal = new Meal(
      id,
      this.mealForm.value.mealTime,
      this.mealForm.value.description,
      this.mealForm.value.mealCalorie
    );

    const next = () => {
      this.mealService.fetchMeals();
    };

    if (this.isEdit) {
      this.mealService.editMeal(meal).subscribe(next);
      alert('Данные обновились!');
    } else {
      this.mealService.addMeal(meal).subscribe(next);
      void this.router.navigate(['']);
    }
  }

  ngOnDestroy(): void {
    this.mealUploadingSubscription.unsubscribe();
  }

}
