import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../../shared/meal.model';
import { Subscription } from 'rxjs';
import { MealService } from '../../shared/meal.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-meal-details',
  templateUrl: './meal-details.component.html',
  styleUrls: ['./meal-details.component.css']
})
export class MealDetailsComponent implements OnInit, OnDestroy {
  @Input() meal!: Meal;
  isRemoving = false;
  mealRemovingSubscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private mealService: MealService,
  ) { }

  ngOnInit(): void {
    this.mealRemovingSubscription = this.mealService.mealRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
  }

  onRemove() {
    this.mealService.removeMeal(this.meal.id).subscribe(() => {
      this.mealService.fetchMeals();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }

  ngOnDestroy() {
    this.mealRemovingSubscription.unsubscribe();
  }
}
